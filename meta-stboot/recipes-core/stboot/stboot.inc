SUMMARY = "stboot minimal systemd implementation"
HOMEPAGE = "https://git.glasklar.is/zaolin/stboot-systemd"

DESCRIPTION = "stboot is a minimal systemd implementation for the stboot project."

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://src/${GO_IMPORT}/LICENSE;md5=3d8d8c074b95e304fd03f2b6c6d00efd"

GO_IMPORT = "git.glasklar.is/zaolin/stboot-systemd"
SRCREV = "db0ebcf124477124dd532882407bddccbdb2c6ae"
SRCBRANCH = "master"
SRC_URI = "git://${GO_IMPORT};protocol=https;branch=${SRCBRANCH} \
           file://host_configuration.json \
           file://trust_policy.json \
           file://isrgrootx1.pem \
           file://ospkg_signing_root.pem \
           "

GO_INSTALL = "${GO_IMPORT}"
do_compile[network] = "1"

GO_LINKSHARED = ""
GO_EXTRA_LDFLAGS = "-w"
GOBUILDFLAGS:remove = "-buildmode=pie"

inherit go-mod
