require stboot.inc

do_install() {
    go_do_install

    mkdir -p ${D}${sysconfdir}
    install -Dm 0644 ${WORKDIR}/host_configuration.json ${D}${sysconfdir}/
    mkdir -p ${D}${sysconfdir}/trust_policy
    install -Dm 0644 ${WORKDIR}/trust_policy.json ${D}${sysconfdir}/trust_policy/
    install -Dm 0644 ${WORKDIR}/ospkg_signing_root.pem ${D}${sysconfdir}/trust_policy/
    install -Dm 0644 ${WORKDIR}/isrgrootx1.pem ${D}${sysconfdir}/trust_policy/
}