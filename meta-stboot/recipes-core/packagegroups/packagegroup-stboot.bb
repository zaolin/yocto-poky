DESCRIPTION = "stboot tiny packagegroup"
SUMMARY = "stboot packagegroup"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

VIRTUAL-RUNTIME_dev_manager := "systemd"

RDEPENDS_${PN} = " \
                  base-files \
                  base-passwd \
                  ${VIRTUAL-RUNTIME_base-utils} \
                  ${VIRTUAL-RUNTIME_init_manager} \
                  ${VIRTUAL-RUNTIME_dev_manager} \
"