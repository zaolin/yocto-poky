SUMMARY = "stboot image"
LICENSE = "MIT"

inherit image

IMAGE_FEATURES += "stateless-rootfs empty-root-password serial-autologin-root"
IMAGE_INSTALL += "kexec-tools busybox systemd stboot"